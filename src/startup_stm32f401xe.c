#define WEAKALIAS __attribute__ ((weak, alias ("Default_Handler")))

extern int main(void);
extern void SystemInit();
extern void __libc_init_array();
void Reset_Handler(void);
__attribute__ ((weak, used)) void Default_Handler(void);

WEAKALIAS void NMI_Handler(void); 
WEAKALIAS void HardFault_Handler(void); 
WEAKALIAS void MemManage_Handler(void); 
WEAKALIAS void BusFault_Handler(void); 
WEAKALIAS void UsageFault_Handler(void); 
WEAKALIAS void SVC_Handler(void); 
WEAKALIAS void DebugMon_Handler(void); 
WEAKALIAS void PendSV_Handler(void); 
WEAKALIAS void SysTick_Handler(void); 

/* External Interrupts */
WEAKALIAS void WWDG_IRQHandler(void);                    /* Window WatchDog              */
WEAKALIAS void PVD_IRQHandler(void);                     /* PVD through EXTI Line detection */
WEAKALIAS void TAMP_STAMP_IRQHandler(void);              /* Tamper and TimeStamps through the EXTI line */
WEAKALIAS void RTC_WKUP_IRQHandler(void);                /* RTC Wakeup through the EXTI line */
WEAKALIAS void FLASH_IRQHandler(void);                   /* FLASH                        */
WEAKALIAS void RCC_IRQHandler(void);                     /* RCC                          */
WEAKALIAS void EXTI0_IRQHandler(void);                   /* EXTI Line0                   */
WEAKALIAS void EXTI1_IRQHandler(void);                   /* EXTI Line1                   */
WEAKALIAS void EXTI2_IRQHandler(void);                   /* EXTI Line2                   */
WEAKALIAS void EXTI3_IRQHandler(void);                   /* EXTI Line3                   */
WEAKALIAS void EXTI4_IRQHandler(void);                   /* EXTI Line4                   */
WEAKALIAS void DMA1_Stream0_IRQHandler(void);            /* DMA1 Stream 0                */
WEAKALIAS void DMA1_Stream1_IRQHandler(void);            /* DMA1 Stream 1                */
WEAKALIAS void DMA1_Stream2_IRQHandler(void);            /* DMA1 Stream 2                */
WEAKALIAS void DMA1_Stream3_IRQHandler(void);            /* DMA1 Stream 3                */
WEAKALIAS void DMA1_Stream4_IRQHandler(void);            /* DMA1 Stream 4                */
WEAKALIAS void DMA1_Stream5_IRQHandler(void);            /* DMA1 Stream 5                */
WEAKALIAS void DMA1_Stream6_IRQHandler(void);            /* DMA1 Stream 6                */
WEAKALIAS void ADC_IRQHandler(void);                     /* ADC1, ADC2 and ADC3s         */
WEAKALIAS void EXTI9_5_IRQHandler(void);                 /* External Line[9:5]s          */
WEAKALIAS void TIM1_BRK_TIM9_IRQHandler(void);           /* TIM1 Break and TIM9          */
WEAKALIAS void TIM1_UP_TIM10_IRQHandler(void);           /* TIM1 Update and TIM10        */
WEAKALIAS void TIM1_TRG_COM_TIM11_IRQHandler(void);      /* TIM1 Trigger and Commutation and TIM11 */
WEAKALIAS void TIM1_CC_IRQHandler(void);                 /* TIM1 Capture Compare         */
WEAKALIAS void TIM2_IRQHandler(void);                    /* TIM2                         */
WEAKALIAS void TIM3_IRQHandler(void);                    /* TIM3                         */
WEAKALIAS void TIM4_IRQHandler(void);                    /* TIM4                         */
WEAKALIAS void I2C1_EV_IRQHandler(void);                 /* I2C1 Event                   */
WEAKALIAS void I2C1_ER_IRQHandler(void);                 /* I2C1 Error                   */
WEAKALIAS void I2C2_EV_IRQHandler(void);                 /* I2C2 Event                   */
WEAKALIAS void I2C2_ER_IRQHandler(void);                 /* I2C2 Error                   */
WEAKALIAS void SPI1_IRQHandler(void);                    /* SPI1                         */
WEAKALIAS void SPI2_IRQHandler(void);                    /* SPI2                         */
WEAKALIAS void USART1_IRQHandler(void);                  /* USART1                       */
WEAKALIAS void USART2_IRQHandler(void);                  /* USART2                       */
WEAKALIAS void EXTI15_10_IRQHandler(void);               /* External Line[15:10]s        */
WEAKALIAS void RTC_Alarm_IRQHandler(void);               /* RTC Alarm (A and B) through EXTI Line */
WEAKALIAS void OTG_FS_WKUP_IRQHandler(void);             /* USB OTG FS Wakeup through EXTI line */
WEAKALIAS void DMA1_Stream7_IRQHandler(void);            /* DMA1 Stream7                 */
WEAKALIAS void SDIO_IRQHandler(void);                    /* SDIO                         */
WEAKALIAS void TIM5_IRQHandler(void);                    /* TIM5                         */
WEAKALIAS void SPI3_IRQHandler(void);                    /* SPI3                         */
WEAKALIAS void DMA2_Stream0_IRQHandler(void);            /* DMA2 Stream 0                */
WEAKALIAS void DMA2_Stream1_IRQHandler(void);            /* DMA2 Stream 1                */
WEAKALIAS void DMA2_Stream2_IRQHandler(void);            /* DMA2 Stream 2                */
WEAKALIAS void DMA2_Stream3_IRQHandler(void);            /* DMA2 Stream 3                */
WEAKALIAS void DMA2_Stream4_IRQHandler(void);            /* DMA2 Stream 4                */
WEAKALIAS void OTG_FS_IRQHandler(void);                  /* USB OTG FS                   */
WEAKALIAS void DMA2_Stream5_IRQHandler(void);            /* DMA2 Stream 5                */
WEAKALIAS void DMA2_Stream6_IRQHandler(void);            /* DMA2 Stream 6                */
WEAKALIAS void DMA2_Stream7_IRQHandler(void);            /* DMA2 Stream 7                */
WEAKALIAS void USART6_IRQHandler(void);                  /* USART6                       */
WEAKALIAS void I2C3_EV_IRQHandler(void);                 /* I2C3 event                   */
WEAKALIAS void I2C3_ER_IRQHandler(void);                 /* I2C3 error                   */
WEAKALIAS void FPU_IRQHandler(void);                     /* FPU                          */
WEAKALIAS void SPI4_IRQHandler(void);                    /* SPI4                         */


// variables defined by the linker script
extern unsigned int _sidata;   // start adress of .data initialization values in flash
extern unsigned int _sdata;    // start of .data section in RAM
extern unsigned int _edata;    // end of .data section in RAM
extern unsigned int _sbss;     // start of .bss section in RAM
extern unsigned int _ebss;     // end of .bss section in RAM

/* _estack is set by the linker script to be the highest address in RAM
 * and in reality is not really the address of any function
 *
 * This is a peculiarity of the Arm Cortex-M architecture, where the initial
 * stack pointer value is loaded from the first entry of the ISR vector table
 * in flash by hardware. The initial stack pointer is not set in by software!
 *
 * The declaration of _estack here as a function is simply a trick
 * to make the C data type compatible for inclusion as a function pointer
 * in vectors[] below
 */
extern void _estack(void);

/* An alternative implementation of the same thing, this forces the value
 * of estack to be placed in the section called .isp
 * The linker script needs to be modified for this to include a section
 * .isp at address 0x00000000
    extern unsigned int _estack;
    __attribute__ ((used, section (".isp"))) unsigned int *estack = &_estack;
*/

__attribute__ ((used, section (".isr_vector")))
void (* const vectors[])(void) = {
    &_estack,
    Reset_Handler,
    NMI_Handler,
    HardFault_Handler,
    MemManage_Handler,
    BusFault_Handler,
    UsageFault_Handler,
    0,
    0,
    0,
    0,
    SVC_Handler,
    DebugMon_Handler,
    0,
    PendSV_Handler,
    SysTick_Handler,

    /* External Interrupts */
    WWDG_IRQHandler,                   /* Window WatchDog              */
    PVD_IRQHandler,                    /* PVD through EXTI Line detection */
    TAMP_STAMP_IRQHandler,             /* Tamper and TimeStamps through the EXTI line */
    RTC_WKUP_IRQHandler,               /* RTC Wakeup through the EXTI line */
    FLASH_IRQHandler,                  /* FLASH                        */
    RCC_IRQHandler,                    /* RCC                          */
    EXTI0_IRQHandler,                  /* EXTI Line0                   */
    EXTI1_IRQHandler,                  /* EXTI Line1                   */
    EXTI2_IRQHandler,                  /* EXTI Line2                   */
    EXTI3_IRQHandler,                  /* EXTI Line3                   */
    EXTI4_IRQHandler,                  /* EXTI Line4                   */
    DMA1_Stream0_IRQHandler,           /* DMA1 Stream 0                */
    DMA1_Stream1_IRQHandler,           /* DMA1 Stream 1                */
    DMA1_Stream2_IRQHandler,           /* DMA1 Stream 2                */
    DMA1_Stream3_IRQHandler,           /* DMA1 Stream 3                */
    DMA1_Stream4_IRQHandler,           /* DMA1 Stream 4                */
    DMA1_Stream5_IRQHandler,           /* DMA1 Stream 5                */
    DMA1_Stream6_IRQHandler,           /* DMA1 Stream 6                */
    ADC_IRQHandler,                    /* ADC1, ADC2 and ADC3s         */
    0,               				  /* Reserved                      */
    0,              					  /* Reserved                     */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    EXTI9_5_IRQHandler,                /* External Line[9:5]s          */
    TIM1_BRK_TIM9_IRQHandler,          /* TIM1 Break and TIM9          */
    TIM1_UP_TIM10_IRQHandler,          /* TIM1 Update and TIM10        */
    TIM1_TRG_COM_TIM11_IRQHandler,     /* TIM1 Trigger and Commutation and TIM11 */
    TIM1_CC_IRQHandler,                /* TIM1 Capture Compare         */
    TIM2_IRQHandler,                   /* TIM2                         */
    TIM3_IRQHandler,                   /* TIM3                         */
    TIM4_IRQHandler,                   /* TIM4                         */
    I2C1_EV_IRQHandler,                /* I2C1 Event                   */
    I2C1_ER_IRQHandler,                /* I2C1 Error                   */
    I2C2_EV_IRQHandler,                /* I2C2 Event                   */
    I2C2_ER_IRQHandler,                /* I2C2 Error                   */
    SPI1_IRQHandler,                   /* SPI1                         */
    SPI2_IRQHandler,                   /* SPI2                         */
    USART1_IRQHandler,                 /* USART1                       */
    USART2_IRQHandler,                 /* USART2                       */
    0,               				  /* Reserved                       */
    EXTI15_10_IRQHandler,              /* External Line[15:10]s        */
    RTC_Alarm_IRQHandler,              /* RTC Alarm (A and B) through EXTI Line */
    OTG_FS_WKUP_IRQHandler,            /* USB OTG FS Wakeup through EXTI line */
    0,                                 /* Reserved     				  */
    0,                                 /* Reserved       			  */
    0,                                 /* Reserved 					  */
    0,                                 /* Reserved                     */
    DMA1_Stream7_IRQHandler,           /* DMA1 Stream7                 */
    0,                                 /* Reserved                     */
    SDIO_IRQHandler,                   /* SDIO                         */
    TIM5_IRQHandler,                   /* TIM5                         */
    SPI3_IRQHandler,                   /* SPI3                         */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    DMA2_Stream0_IRQHandler,           /* DMA2 Stream 0                */
    DMA2_Stream1_IRQHandler,           /* DMA2 Stream 1                */
    DMA2_Stream2_IRQHandler,           /* DMA2 Stream 2                */
    DMA2_Stream3_IRQHandler,           /* DMA2 Stream 3                */
    DMA2_Stream4_IRQHandler,           /* DMA2 Stream 4                */
    0,                    			  /* Reserved                     */
    0,              					  /* Reserved                     */
    0,              					  /* Reserved                     */
    0,             					  /* Reserved                     */
    0,              					  /* Reserved                     */
    0,              					  /* Reserved                     */
    OTG_FS_IRQHandler,                 /* USB OTG FS                   */
    DMA2_Stream5_IRQHandler,           /* DMA2 Stream 5                */
    DMA2_Stream6_IRQHandler,           /* DMA2 Stream 6                */
    DMA2_Stream7_IRQHandler,           /* DMA2 Stream 7                */
    USART6_IRQHandler,                 /* USART6                       */
    I2C3_EV_IRQHandler,                /* I2C3 event                   */
    I2C3_ER_IRQHandler,                /* I2C3 error                   */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    FPU_IRQHandler,                    /* FPU                          */
    0,                                 /* Reserved                     */
    0,                                 /* Reserved                     */
    SPI4_IRQHandler,                   /* SPI4                         */
};

void Reset_Handler(void) {
    unsigned int *src, *dst;
    
    // copy the initialized data section from flash to RAM
    src = &_sidata;
    dst = &_sdata;
    while (dst < &_edata) *dst++ = *src++;

    // zero out the uninitialized data section
    dst = &_sbss;
    while (dst < &_ebss) *dst++ = 0;

    // configure system as defined in system_stm32f4xx.c
    // this includes setting the ISR vector table address,
    // configuring the clock system and enabling the FPU
    SystemInit();
    // call libc constructors
    __libc_init_array();
    // branch to main
    main();

    while (1);
}

void Default_Handler(void) {
    while (1);
}
